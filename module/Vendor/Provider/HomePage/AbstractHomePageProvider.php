<?php


namespace Module\Vendor\Provider\HomePage;


abstract class AbstractHomePageProvider
{
    abstract public function title();

    abstract public function action();
}

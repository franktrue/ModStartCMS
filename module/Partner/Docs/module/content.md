
### 使用说明

需要使用的 `blade` 模板中直接引入


**常规版本**

```
@include('module::Partner.View.pc.public.partner',['position'=>'home'])
``` 


**透明版本**

```
@include('module::Partner.View.pc.public.partnerTransparent',['position'=>'home'])
``` 

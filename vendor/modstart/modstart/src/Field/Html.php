<?php


namespace ModStart\Field;


use ModStart\Core\Util\ConvertUtil;

class Html extends AbstractField
{
    protected $html = '';
    protected $plain = false;
    protected $isLayoutField=true;

    public function html($html)
    {
        $this->html = $html;
        return $this;
    }

    public function plain()
    {
        $this->plain = true;
        return $this;
    }

    public function render()
    {
        if ($this->html instanceof \Closure) {
            $this->html = ConvertUtil::render(
                $this->html->call($this->variables(), $this->context)
            );
        }
        if ($this->plain) {
            return $this->html;
        }
        return <<<EOT
<div class="line">
    <div class="label">
        {$this->label}:
    </div>
    <div class="field">
        $this->html
    </div>
</div>
EOT;
    }

}

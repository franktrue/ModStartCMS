<?php

namespace ModStart;

use Illuminate\Support\Facades\Cache;
use ModStart\Core\Exception\BizException;
use ModStart\Form\Form;
use ModStart\Support\Manager\FieldManager;
use ModStart\Support\Manager\WidgetManager;

/**
 * Class ModStart
 * @package ModStart
 */
class ModStart
{
    public static $version = '1.8.0';

    public static $script = [];
    public static $style = [];
    public static $css = [];
    public static $js = [];

    /**
     * 清除缓存
     */
    public static function clearCache()
    {
        Cache::forget('ModStartServiceProviders');
        Cache::forget('ModStartAdminRoutes');
        Cache::forget('ModStartApiRoutes');
        Cache::forget('ModStartOpenApiRoutes');
        Cache::forget('ModStartWebRoutes');
        /**
         * 如果启用了Laravel优化，这些文件会缓存ServiceProvider
         * 会造成缓存清理不干净甚至服务崩溃的问题
         */
        self::safeCleanOptimizedFile('bootstrap/cache/compiled.php');
        self::safeCleanOptimizedFile('bootstrap/cache/services.json');
        self::safeCleanOptimizedFile('bootstrap/cache/config.php');
    }

    private static function safeCleanOptimizedFile($file)
    {
        if (file_exists($path = base_path($file))) {
            @unlink($path);
        }
    }


    public static function scriptFile($scriptFile)
    {
        if (strpos($scriptFile, '/') !== 0) {
            $scriptFile = base_path($scriptFile);
        }
        try {
            return self::script(file_get_contents($scriptFile));
        } catch (\Exception $e) {
            BizException::throws('FileNotFound -> ' . $scriptFile);
        }
    }

    /**
     * JS 脚本代码，显示在body底部
     * @param string $script
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function script($script = '')
    {
        $script = trim($script);
        if (!empty($script)) {
            self::$script = array_merge(self::$script, (array)$script);
            return;
        }
        return view('modstart::part.script', ['script' => array_unique(self::$script)]);
    }

    public static function styleFile($styleFile)
    {
        if (strpos($styleFile, '/') !== 0) {
            $styleFile = base_path($styleFile);
        }
        try {
            return self::style(file_get_contents($styleFile));
        } catch (\Exception $e) {
            BizException::throws('FileNotFound -> ' . $styleFile);
        }
    }

    /**
     * CSS 样式代码，显示在head头部
     * @param string $style
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function style($style = '')
    {
        $style = trim($style);
        if (!empty($style)) {
            self::$style = array_merge(self::$style, (array)$style);
            return;
        }
        static::$style = array_merge(
            static::$style,
            FieldManager::collectFieldAssets('style'),
            WidgetManager::collectWidgetAssets('style')
        );
        return view('modstart::part.style', ['style' => array_unique(self::$style)]);
    }

    /**
     * CSS 样式文件，显示在head头部
     * @param null $css
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function css($css = null)
    {
        if (!is_null($css)) {
            self::$css = array_merge(self::$css, (array)$css);
            return;
        }
        static::$css = array_merge(
            static::$css,
            FieldManager::collectFieldAssets('css')
        );
        return view('modstart::part.css', ['css' => array_unique(static::$css)]);
    }

    /**
     * JS 脚本文件，显示在body底部
     * @param null $js
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public static function js($js = null)
    {
        if (!is_null($js)) {
            self::$js = array_merge(self::$js, (array)$js);
            return;
        }
        static::$js = array_merge(
            static::$js,
            FieldManager::collectFieldAssets('js')
        );
        return view('modstart::part.js', ['js' => array_unique(static::$js)]);
    }
}

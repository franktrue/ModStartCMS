<?php

namespace App\Constant;

class AppConstant
{
    const APP = 'cms';
    const VERSION = '2.1.0';
}
